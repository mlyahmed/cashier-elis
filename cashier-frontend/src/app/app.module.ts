import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {NavigationActionTiming, StoreRouterConnectingModule} from '@ngrx/router-store';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './layout/header/header.component';
import {LeftMenuComponent} from './layout/left-menu/left-menu.component';
import {FooterComponent} from './layout/footer/footer.component';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DashboardComponent} from './stories/dashboard/dashboard.component';
import {ChartsModule} from 'ng2-charts';
import {CashInComponent} from './stories/cash-in/cash-in.component';
import {OAuthModule} from 'angular-oauth2-oidc';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginComponent} from './layout/hooks/login/login.component';
import {AuthInterceptor} from './security/interceptors/auth.interceptor';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {ShowIfLoggedInDirective} from './security/directives/show-if-logged-in.directive';
import {EffectsModule} from '@ngrx/effects';
import {UserEffects} from './domain/user/store/users.effects';
import {CustomSerializer, metaReducers, reducers} from './app.reducers';
import {LogoutComponent} from './layout/hooks/logout/logout.component';
import {SsoComponent} from './layout/hooks/sso/sso.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftMenuComponent,
    FooterComponent,
    DashboardComponent,
    CashInComponent,
    LoginComponent,
    ShowIfLoggedInDirective,
    LogoutComponent,
    SsoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FontAwesomeModule,
    ChartsModule,
    StoreRouterConnectingModule.forRoot({
      serializer: CustomSerializer,
      navigationActionTiming: NavigationActionTiming.PostActivation
    }),
    OAuthModule.forRoot(),
    EffectsModule.forRoot([UserEffects]),
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument() : []
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
