import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {UserService} from '../../domain/user/service/user.service';
import {OAuthService} from 'angular-oauth2-oidc';
import {AppState, isUserAuthenticated} from '../../app.reducers';
import {Store} from '@ngrx/store';
import {switchMap} from 'rxjs/operators';
import {fromPromise} from 'rxjs/internal-compatibility';

@Injectable({
  providedIn: 'root'
})
export class IsAuthenticatedGuard implements CanActivate {

  constructor(private oauthService: OAuthService,
              private authService: UserService,
              private router: Router,
              private store: Store<AppState>
  ) {

  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.store.select(isUserAuthenticated).pipe(
      // @ts-ignore
      switchMap(auth => {
        if (auth) {
          return Observable.of(true);
        }

        return fromPromise(this.oauthService.loadDiscoveryDocumentAndTryLogin().then( (value: object) => {
          // https://github.com/manfredsteyer/angular-oauth2-oidc/issues/352
          const success: boolean = Boolean(value);
          if (!success) {
            return this.router.parseUrl('/login');
          }
          return true;
        }));
      })
    );

  }

}
