import {Directive, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {Observable} from 'rxjs';
import {select, Store} from '@ngrx/store';
import {AppState, isUserAuthenticated} from '../../app.reducers';

@Directive({
  selector: '[showIfLoggedIn]'
})
export class ShowIfLoggedInDirective implements OnInit {

  isAuthenticated$: Observable<boolean>;
  show: boolean;

  constructor(private store: Store<AppState>,
              private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef) {
    this.isAuthenticated$ = this.store.pipe(select(isUserAuthenticated));
  }

  @Input() set showIfLoggedIn(show: boolean) {
    this.show = show;
  }

  ngOnInit(): void {
    this.isAuthenticated$.subscribe((isAuth: boolean) => {
      if (isAuth && this.show || !isAuth && !this.show) {
        this.viewContainer.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainer.clear();
      }
    });
  }

}

