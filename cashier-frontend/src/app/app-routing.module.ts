import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './stories/dashboard/dashboard.component';
import {CashInComponent} from './stories/cash-in/cash-in.component';
import {IsAuthenticatedGuard} from './security/guards/is-authenticated.guard';
import {LoginComponent} from './layout/hooks/login/login.component';
import {LogoutComponent} from './layout/hooks/logout/logout.component';
import {SsoComponent} from './layout/hooks/sso/sso.component';

const routes: Routes = [
  { path: 'sso', component: SsoComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [IsAuthenticatedGuard] },
  { path: 'cash-in', component: CashInComponent, canActivate: [IsAuthenticatedGuard] },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
