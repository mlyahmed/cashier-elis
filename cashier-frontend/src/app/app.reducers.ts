import {createSelector} from 'reselect';
import {ActionReducerMap, createFeatureSelector, MetaReducer} from '@ngrx/store';
import {routerReducer, RouterReducerState, RouterStateSerializer} from '@ngrx/router-store';
import {Params, RouterStateSnapshot} from '@angular/router';

import {environment} from '../environments/environment';
import * as users from './domain/user/store/users.reducers';
import {UserState} from './domain/user/store/users.reducers';
import {Injectable} from '@angular/core';


export interface AppState {
  users: users.UserState;
  router: RouterReducerState<RouterStateUrl>;
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];

export const reducers: ActionReducerMap<AppState> = {
  users: users.usersReducer,
  router: routerReducer
};


export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
}

@Injectable()
export class CustomSerializer implements RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const {
      url,
      root: { queryParams }
    } = routerState;
    const { params } = route;

    // Only return an object including the URL, params and query params instead of the entire snapshot
    return { url, params, queryParams };
  }
}

export const selectUserState = createFeatureSelector<UserState>('users');
export const getUser = createSelector(selectUserState, users.getAuthenticatedUser);
export const isUserAuthenticated = createSelector(selectUserState, users.isAuthenticated);

