import {Component, OnInit} from '@angular/core';
import {SignOutAction} from '../../../domain/user/store/users.actions';
import {AppState} from '../../../app.reducers';
import {Store} from '@ngrx/store';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.store.dispatch(new SignOutAction({}));
  }

}
