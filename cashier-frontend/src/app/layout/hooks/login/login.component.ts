import {Component, OnInit} from '@angular/core';
import {AppState, isUserAuthenticated} from '../../../app.reducers';
import {select, Store} from '@ngrx/store';
import {UserService} from '../../../domain/user/service/user.service';
import {Router} from '@angular/router';
import {merge} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private store: Store<AppState>,
              private authService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    console.log(`LoginComponent............`);

    const reAuth$ = this.store.pipe(select(isUserAuthenticated))
      .filter((isAuth) => isAuth === false)
      .do (() => this.authService.launchAuthentication());

    const goToDashboard$ = this.store.pipe(select(isUserAuthenticated))
      .filter((isAuth) => isAuth === true)
      .do (() => this.router.navigate(['/dashboard']));

    merge(reAuth$, goToDashboard$).subscribe();
  }

}
