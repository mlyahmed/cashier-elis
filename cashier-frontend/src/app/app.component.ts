import {Component, OnDestroy, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {AppState, isUserAuthenticated} from './app.reducers';
import {AuthConfig, JwksValidationHandler, OAuthEvent, OAuthService} from 'angular-oauth2-oidc';
import {AuthenticatedAction} from './domain/user/store/users.actions';
import {delay, takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  isAuthenticated = false;

  private authConfig: AuthConfig = {
    redirectUri: 'http://localhost:4200/sso',
    postLogoutRedirectUri: 'http://localhost:4200/logout',
    logoutUrl: 'http://localhost:4200/logout',
    clientId: '0oajfbzuwnhHMzYKR0h7',
    scope: 'openid profile email',
    issuer: 'https://dev-693491.oktapreview.com/oauth2/default',
    clearHashAfterLogin: true,
    showDebugInformation: true
  };

  constructor(private oauthService: OAuthService,
              private store: Store<AppState>) {

    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.events.subscribe(({ type }: OAuthEvent) => {
      console.log(`OAuthEvent : ${type}`)
      if (type === 'token_received') {
        this.store.dispatch(new AuthenticatedAction({token: this.oauthService.getIdToken()}));
      }
    });

  }

  ngOnInit(): void {
    this.store.pipe(
      select(isUserAuthenticated),
      takeUntil(this.unsubscribe$)
    )
      .subscribe(isAuth => this.isAuthenticated = isAuth);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
