import { Component, OnInit } from '@angular/core';
import {OperationService} from '../../domain/operation/service/operation.service';
import {Operation} from '../../domain/operation/operation';

@Component({
  selector: 'app-cash-in',
  templateUrl: './cash-in.component.html',
  styleUrls: ['./cash-in.component.scss']
})
export class CashInComponent implements OnInit {

  constructor(private operationService: OperationService) { }

  ngOnInit() {
  }

  public cashIn($event: MouseEvent) {
    const operation: Operation = {};
    this.operationService.cashIn(operation).subscribe((operation: Operation) => {
      console.log('Operation : ' + operation);
    });
  }
}
