import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppState, getUser} from '../../app.reducers';
import {Store} from '@ngrx/store';
import {User} from '../../domain/user/user';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  user$: Observable<User>;

  public barChartLabels = ['Juillet 2018', 'Août 2018', 'Septembre 2018', 'Novembre 2018', 'Décembre 2018', 'Janvier 2019', 'Février 2019'];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData = [
    {data: [1000, 800, 1300, 1456, 300, 659, 2876], label: 'Encaissements'},
    {data: [300, 230, 40, 235, 800, 123, 300], label: 'Décaissements'}
  ];

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.user$ = this.store.select(getUser);
  }

  public chartClickedBar(e:any): void {
    console.log(e);
  }

  public chartHoveredBar(e:any): void {
    console.log(e);
  }

}
