import {Injectable, OnInit} from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {Observable, of} from 'rxjs';
import {User} from '../user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map, switchMap, tap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService implements OnInit {

  endpoint = '/api/actor';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private oauthService: OAuthService,
              private http: HttpClient
              ) {}

  ngOnInit(): void {
  }

  public launchAuthentication(): void {
    this.oauthService.initImplicitFlow();
  }

  public getAccessToken(): string {
    return this.oauthService.getAccessToken();
  }

  public logout(): Observable<boolean> {
    this.oauthService.logOut(false);
    return Observable.of(true);
  }

  private getCurrentUserDetails(): Observable<User> {
    return this.http.get<User>(this.endpoint + '/v1/current', this.httpOptions).pipe(
      catchError(this.handleError<any>('currentUserDetails'))
    );
  }

  public authenticatedUserDetails(): Observable<User> {
    const claims: any = this.oauthService.getIdentityClaims();

    if (claims) {
      return this.getCurrentUserDetails().pipe(
          map((user: User) => {
            user.email = claims.email;
            user.name = claims.name;
            user.sub = claims.sub;
            return user;
          })
        );
    }

    return Observable.of(null);
  }

  public authenticatedUser(): Observable<User> {
    const claims: any = this.oauthService.getIdentityClaims();

    if (claims) {
      const user: User = new User();
      user.email = claims.email;
      user.name = claims.name;
      user.sub = claims.sub;
      return Observable.of(user);

      /*Observable.of(user).pipe(
        switchMap(usr => this.getCurrentUserDetails().pipe(
          tap(details => console.log(`User details ${JSON.stringify(details)}`)),
          map(details => {
            usr.id = details.id;
            return usr;
          })
        ))
      );*/
    }

    return Observable.of(null);
  }

  private handleError<T>(operation = 'currentUserDetails', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }


}
