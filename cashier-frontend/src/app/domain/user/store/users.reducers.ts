import {User} from '../user';
import {UserActions, UserActionTypes} from './users.actions';

export interface UserState {
  authenticated: boolean;
  error?: string;
  user?: User;
}

const initialState: UserState = {
  authenticated: false,
};

export function usersReducer(state: any = initialState, action: UserActions): UserState {
  switch (action.type) {
    case UserActionTypes.AUTHENTICATED_SUCCESS:
      return Object.assign({}, state, {
        authenticated: action.payload.authenticated,
        user: action.payload.user
      });
    case UserActionTypes.AUTHENTICATED_ERROR:
      return Object.assign({}, state, {
        authenticated: false,
        error: action.payload.error.message,
      });

    case UserActionTypes.LOAD_DETAILS_SUCCESS:
      return Object.assign({}, state, {
        user: action.payload.user
      });

    case UserActionTypes.SIGN_OUT_SUCCESS:
      return Object.assign({}, state, {
        authenticated: false,
        error: undefined,
        user: undefined
      });
    case UserActionTypes.SIGN_OUT_ERROR:
      return Object.assign({}, state, {
        authenticated: true,
        error: action.payload.error.message,
        user: undefined
      });

    default:
      return state;
  }
}

export const isAuthenticated = (state: UserState) => state.authenticated;
export const getAuthenticatedUser = (state: UserState) => state.user;
