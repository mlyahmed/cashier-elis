import {type} from './util';

import { Action } from '@ngrx/store';
import {User} from '../user';

export const UserActionTypes = {
  AUTHENTICATED: type('[users] Authenticated'),
  AUTHENTICATED_ERROR: type('[users] Authenticated error'),
  AUTHENTICATED_SUCCESS: type('[users] Authenticated success'),

  LOAD_DETAILS: type('[users] Load details'),
  LOAD_DETAILS_SUCCESS: type('[users] Load details success'),
  LOAD_DETAILS_ERROR: type('[users] Load details error'),

  SIGN_OUT: type('[users] Sign off'),
  SIGN_OUT_ERROR: type('[users] Sign off error'),
  SIGN_OUT_SUCCESS: type('[users] Sign off success')
};

export class AuthenticatedAction implements Action {
  public type: string = UserActionTypes.AUTHENTICATED;
  constructor(public payload?: {token?: string}) {}
}

export class AuthenticatedSuccessAction implements Action {
  public type: string = UserActionTypes.AUTHENTICATED_SUCCESS;
  constructor(public payload: {authenticated: boolean, user: User}) {}
}

export class AuthenticatedErrorAction implements Action {
  public type: string = UserActionTypes.AUTHENTICATED_ERROR;
  constructor(public payload?: any) {}
}

export class LoadDetailsAction implements Action {
  public type: string = UserActionTypes.LOAD_DETAILS;
  constructor(public payload?: {user: User}) {}
}

export class LoadDetailsSuccessAction implements Action {
  public type: string = UserActionTypes.LOAD_DETAILS_SUCCESS;
  constructor(public payload: {user: User}) {}
}

export class LoadDetailsErrorAction implements Action {
  public type: string = UserActionTypes.LOAD_DETAILS_ERROR;
  constructor(public payload?: any) {}
}


export class SignOutAction implements Action {
  public type: string = UserActionTypes.SIGN_OUT;
  constructor(public payload?: any) {}
}

export class SignOutErrorAction implements Action {
  public type: string = UserActionTypes.SIGN_OUT_ERROR;
  constructor(public payload?: any) {}
}

export class SignOutSuccessAction implements Action {
  public type: string = UserActionTypes.SIGN_OUT_SUCCESS;
  constructor(public payload?: any) {}
}

export type UserActions
  =
  AuthenticatedAction
  | AuthenticatedErrorAction
  | AuthenticatedSuccessAction
  | LoadDetailsAction
  | LoadDetailsSuccessAction
  | SignOutAction
  | SignOutErrorAction
  | SignOutSuccessAction
  ;
