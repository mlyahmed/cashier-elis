import { Injectable } from '@angular/core';

import {Effect, Actions, ofType} from '@ngrx/effects';
import { Action } from '@ngrx/store';

// import rxjs
import { Observable } from 'rxjs/Observable';

import {
  UserActionTypes,
  AuthenticatedErrorAction,
  AuthenticatedSuccessAction,
  SignOutErrorAction,
  SignOutSuccessAction, LoadDetailsAction, LoadDetailsSuccessAction, LoadDetailsErrorAction
} from './users.actions';
import {UserService} from '../service/user.service';
import {catchError, map, switchMap, tap} from 'rxjs/operators';


@Injectable()
export class UserEffects {

  @Effect()
  public authenticated: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.AUTHENTICATED),

      switchMap(() => this.authService.authenticatedUser().pipe(
        tap(usr => console.log(`Effect : UserActionTypes.AUTHENTICATED : ${JSON.stringify(usr)}`)),
        switchMap(usr => [
          new AuthenticatedSuccessAction({ authenticated: (usr !== null), user: usr }),
          new LoadDetailsAction({ user: usr })
        ]),
        catchError(err => Observable.of(new AuthenticatedErrorAction({ error: err })))
      ))
    );


  @Effect()
  public details: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.LOAD_DETAILS),

      switchMap(() => this.authService.authenticatedUserDetails().pipe(
        tap(usr => console.log(`Effect : UserActionTypes.LOAD_DETAILS : ${JSON.stringify(usr)}`)),
        map(usr => new LoadDetailsSuccessAction({ user: usr })),
        catchError(err => Observable.of(new LoadDetailsErrorAction({ error: err })))
      )
      )
    );




  @Effect()
  public signOut: Observable<Action> = this.actions$
    .pipe(
      ofType(UserActionTypes.SIGN_OUT),
      switchMap(() => this.authService.logout()
        .pipe(
          tap(() => console.log(`Effect : UserActionTypes.SIGN_OUT`)),
          map(() => new SignOutSuccessAction()),
          catchError(err => Observable.of(new SignOutErrorAction({ error: err })))
        )
      )
    );

  constructor(
    private actions$: Actions,
    private authService: UserService
  ) { }
}
