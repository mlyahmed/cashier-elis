import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Operation} from '../operation';
import {Observable, of} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class OperationService {

  endpoint = '/api/operation';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  public cashIn(operation: Operation): Observable<Operation> {
    return this.http.post<any>(this.endpoint + '/v1/cash-in', JSON.stringify(operation), this.httpOptions).pipe(
      tap((op) => console.log(`added operation id=${op.id}`)),
      catchError(this.handleError<any>('operation'))
    );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
