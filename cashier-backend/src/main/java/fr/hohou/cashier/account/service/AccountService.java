package fr.hohou.cashier.account.service;

import fr.hohou.cashier.account.domain.Account;

public interface AccountService {

    Account getAccountBoundToActor(String actorId);

}
