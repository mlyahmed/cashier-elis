package fr.hohou.cashier.account.domain;

import fr.hohou.cashier.actor.domain.ActorBean;
import fr.hohou.cashier.amount.Amount;

import java.io.Serializable;

public class AccountBean implements Serializable {

    private String id;
    private ActorBean owner;
    private Amount currentBalance;
    private Amount comingBalance;


    public static AccountBean newAccountBean() {
        return new AccountBean();
    }

    public String getId() {
        return id;
    }

    public AccountBean setId(String id) {
        this.id = id;
        return this;
    }

    public ActorBean getOwner() {
        return owner;
    }

    public AccountBean setOwner(ActorBean owner) {
        this.owner = owner;
        return this;
    }

    public Amount getCurrentBalance() {
        return currentBalance;
    }

    public AccountBean setCurrentBalance(Amount currentBalance) {
        this.currentBalance = currentBalance;
        return this;
    }

    public Amount getComingBalance() {
        return comingBalance;
    }

    public AccountBean setComingBalance(Amount comingBalance) {
        this.comingBalance = comingBalance;
        return this;
    }
}
