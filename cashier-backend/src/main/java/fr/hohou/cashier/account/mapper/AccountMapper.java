package fr.hohou.cashier.account.mapper;

import fr.hohou.cashier.account.domain.Account;
import fr.hohou.cashier.account.domain.AccountBean;

public interface AccountMapper {

    AccountBean toBean(Account account);

}
