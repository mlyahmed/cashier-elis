package fr.hohou.cashier.account.domain;

import fr.hohou.cashier.CashierEntity;
import fr.hohou.cashier.actor.domain.Actor;
import fr.hohou.cashier.amount.Amount;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
public class Account implements CashierEntity {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private String id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "CURRENT_BALANCE", nullable = false)
    @Type(type = AMOUNT_TYPE)
    private Amount currentBalance;

    @Column(name = "COMING_BALANCE", nullable = false)
    @Type(type = AMOUNT_TYPE)
    private Amount comingBalance;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "OWNER_ID")
    private Actor owner;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Amount getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Amount currentBalance) {
        this.currentBalance = currentBalance;
    }

    public Amount getComingBalance() {
        return comingBalance;
    }

    public void setComingBalance(Amount comingBalance) {
        this.comingBalance = comingBalance;
    }

    public Actor getOwner() {
        return owner;
    }

    public void setOwner(Actor owner) {
        this.owner = owner;
    }
}
