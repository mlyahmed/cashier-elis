package fr.hohou.cashier.account.service;

import fr.hohou.cashier.account.dao.AccountDAO;
import fr.hohou.cashier.account.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {


    private final AccountDAO accountDAO;

    @Autowired
    public AccountServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }


    @Override
    public Account getAccountBoundToActor(String actorId) {
        return accountDAO.findByOwnerId(actorId).orElse(null);
    }

}
