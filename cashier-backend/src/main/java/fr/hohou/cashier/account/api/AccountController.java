package fr.hohou.cashier.account.api;

import fr.hohou.cashier.account.domain.AccountBean;
import fr.hohou.cashier.account.mapper.AccountMapper;
import fr.hohou.cashier.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    private final AccountMapper accountMapper;

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountMapper accountMapper, AccountService accountService) {
        this.accountMapper = accountMapper;
        this.accountService = accountService;
    }



    @GetMapping(path = "/v1/by/actorId/{actorId}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(HttpStatus.OK)
    //FIXME : Security, nobody has access to an actor's account except himself and an admin.
    public @ResponseBody
    AccountBean current(@PathVariable String actorId) {
        return accountMapper.toBean(accountService.getAccountBoundToActor(actorId));
    }

}
