package fr.hohou.cashier.account.dao;

import fr.hohou.cashier.account.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountDAO extends JpaRepository<Account, String> {

    Optional<Account> findByOwnerId(String ownerId);

}
