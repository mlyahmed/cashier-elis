package fr.hohou.cashier.account.mapper;

import fr.hohou.cashier.account.domain.Account;
import fr.hohou.cashier.account.domain.AccountBean;
import fr.hohou.cashier.actor.mapper.ActorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountMapperImpl implements AccountMapper {

    private final ActorMapper actorMapper;


    @Autowired
    public AccountMapperImpl(ActorMapper actorMapper) {
        this.actorMapper = actorMapper;
    }


    @Override
    public AccountBean toBean(Account account) {
        return AccountBean.newAccountBean()
                .setId(account.getId())
                .setComingBalance(account.getComingBalance())
                .setCurrentBalance(account.getCurrentBalance())
                .setOwner(actorMapper.toBean(account.getOwner()));
    }

}
