package fr.hohou.cashier.amount;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class AmountJsonDeserializer extends JsonDeserializer<Amount> {
    @Override
    public Amount deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        final TreeNode amount = jsonParser.getCodec().readTree(jsonParser);
        final TextNode value = (TextNode) amount.get("value");
        return Amount.newAmount(value.asText());
    }
}
