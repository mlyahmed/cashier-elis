package fr.hohou.cashier.amount;

public class BadAmountValueException extends RuntimeException {

    BadAmountValueException(final String value) {
        super(String.format("The Value <%s> is not valid", value));
    }

}
