package fr.hohou.cashier.amount;

import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isEmpty;



public class Amount {


    public static final Amount ZERO_AMOUNT = new Amount("0");

    private final BigDecimal delegate;

    public static Amount newAmount(String value) {
        return new Amount(value);
    }

    protected Amount(String value) {
        delegate = scale(parse(value));
    }

    public String asString() {
        return delegate.toString();
    }

    double asDouble() {
        return delegate.doubleValue();
    }

    public Amount plus(Amount amount) {
        return new Amount(delegate.add(parse(amount.asString())).toString());
    }

    public Amount multiply(int times) {
        return multiply(Integer.toString(times));
    }

    public Amount multiply(double times) {
        return multiply(Double.toString(times));
    }

    private Amount multiply(String times) {
        return new Amount(delegate.multiply(parse(times)).toString());
    }

    private BigDecimal parse(String value) {
        return isEmpty(value) ? BigDecimal.ZERO : createBigDecimal(value);
    }

    private BigDecimal scale(BigDecimal value) {
        return value.setScale(2, RoundingMode.HALF_UP);
    }

    private BigDecimal createBigDecimal(String value) {
        if (!NumberUtils.isParsable(value)) throw new BadAmountValueException(value);
        return NumberUtils.createBigDecimal(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Amount amount = (Amount) o;
        return Objects.equals(asString(), amount.asString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.asString());
    }

    @Override
    public String toString() {
        return String.format("Amount(%s)", asString());
    }
}
