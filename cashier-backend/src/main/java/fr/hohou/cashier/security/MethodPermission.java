package fr.hohou.cashier.security;

import java.util.Arrays;

public enum MethodPermission {
    CREATE, READ, UPDATE, DELETE;

    public static MethodPermission fromObject(Object p) {
        if (p == null) return null;
        if (p instanceof MethodPermission) return (MethodPermission) p;
        return Arrays.stream(MethodPermission.values()).filter(v -> v.name().equals(p.toString())).findFirst().orElse(null);
    }
}
