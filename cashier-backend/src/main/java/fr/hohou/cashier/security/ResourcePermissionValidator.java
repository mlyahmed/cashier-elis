package fr.hohou.cashier.security;

import org.springframework.security.core.Authentication;

public interface ResourcePermissionValidator<T> {

    boolean hasPermission(Authentication authentication, T target, MethodPermission permission);

    Class<T> support();

}
