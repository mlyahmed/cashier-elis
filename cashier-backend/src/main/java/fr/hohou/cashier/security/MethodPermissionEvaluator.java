package fr.hohou.cashier.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class MethodPermissionEvaluator implements PermissionEvaluator {

    private Map<Class<?>, ResourcePermissionValidator> validators = new LinkedHashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public boolean hasPermission(final Authentication authentication, final Object target, final Object permission) {
        final ResourcePermissionValidator validator = validators.get(target.getClass());
        if (validator != null) return validator.hasPermission(authentication, target, MethodPermission.fromObject(permission));
        return false;
    }

    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String target, final Object permission) {
        return false;
    }

    @Autowired(required = false)
    public void setBusinessEntityPermissionValidators(final List<ResourcePermissionValidator> validators) {
        validators.forEach(v -> this.validators.put(v.support(), v));
    }

}
