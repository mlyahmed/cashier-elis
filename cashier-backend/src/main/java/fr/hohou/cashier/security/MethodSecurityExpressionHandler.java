package fr.hohou.cashier.security;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.expression.spel.support.StandardTypeLocator;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.core.Authentication;

public class MethodSecurityExpressionHandler extends DefaultMethodSecurityExpressionHandler {

    @Override
    public StandardEvaluationContext createEvaluationContextInternal(Authentication auth, MethodInvocation mi) {
        final StandardEvaluationContext evaluationContext = super.createEvaluationContextInternal(auth, mi);
        final StandardTypeLocator typeLocator = (StandardTypeLocator) evaluationContext.getTypeLocator();
        typeLocator.registerImport(this.getClass().getPackage().getName());
        return evaluationContext;
    }

}
