package fr.hohou.cashier.actor.mapper;

import fr.hohou.cashier.actor.domain.Actor;
import fr.hohou.cashier.actor.domain.ActorBean;

public interface ActorMapper {

    ActorBean toBean(Actor actor);

}
