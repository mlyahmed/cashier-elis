package fr.hohou.cashier.actor.api;

import fr.hohou.cashier.actor.service.ActorService;
import fr.hohou.cashier.actor.domain.ActorBean;
import fr.hohou.cashier.actor.mapper.ActorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/actor")
public class ActorController {

    private final ActorMapper actorMapper;

    private final ActorService actorService;

    @Autowired
    public ActorController(ActorMapper actorMapper, ActorService actorService) {
        this.actorMapper = actorMapper;
        this.actorService = actorService;
    }


    @GetMapping(path = "/v1/current", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(HttpStatus.OK)
    //FIXME : Security, nobody has access to an actor details except himself and ans admin.
    public @ResponseBody
    ActorBean current() {
        return actorMapper.toBean(actorService.getCurrentActorDetails());
    }

}
