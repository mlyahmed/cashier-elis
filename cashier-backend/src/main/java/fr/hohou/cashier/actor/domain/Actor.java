package fr.hohou.cashier.actor.domain;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import java.util.Set;

@Entity
@Table(name = "ACTOR")
public class Actor {

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    private String id;

    @Column(name = "EMAIL", nullable = false, unique = true)
    private String email;

    @ElementCollection(targetClass = ActorRole.class, fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name = "ACTOR_ROLE", joinColumns = @JoinColumn(name = "ACTOR_ID"))
    @Column(name = "ROLE_CODE")
    private Set<ActorRole> roles;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<ActorRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<ActorRole> roles) {
        this.roles = roles;
    }
}
