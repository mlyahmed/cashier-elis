package fr.hohou.cashier.actor.domain;

import java.io.Serializable;

public class ActorBean implements Serializable {

    private String id;
    private String email;


    public static ActorBean newActorBean() {
        return new ActorBean();
    }


    public String getId() {
        return id;
    }

    public ActorBean setId(String id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public ActorBean setEmail(String email) {
        this.email = email;
        return this;
    }
}
