package fr.hohou.cashier.actor.service;

import fr.hohou.cashier.actor.domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorServiceImpl implements ActorService {

    private final CurrentActor currentActor;

    @Autowired
    public ActorServiceImpl(CurrentActor currentActor) {
        this.currentActor = currentActor;
    }


    @Override
    public Actor getCurrentActorDetails() {
        return this.currentActor.getActor();
    }

}
