package fr.hohou.cashier.actor.dao;

import fr.hohou.cashier.actor.domain.Actor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ActorDAO extends JpaRepository<Actor, String> {

    Optional<Actor> findByEmail(String email);

}
