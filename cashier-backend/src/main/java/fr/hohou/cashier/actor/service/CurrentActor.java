package fr.hohou.cashier.actor.service;

import fr.hohou.cashier.actor.dao.ActorDAO;
import fr.hohou.cashier.actor.domain.Actor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Map;

@Component
public class CurrentActor {

    private static final String[] EMAIL_KEYS = new String[] {"sub", "user_name"};

    private final ActorDAO actorDAO;

    private final TokenStore tokenStore;


    public CurrentActor(ActorDAO actorDAO, TokenStore tokenStore) {
        this.actorDAO = actorDAO;
        this.tokenStore = tokenStore;
    }

    public String getEmail() {
        return isAuthenticated() ? extractEmail() : null;
    }


    public Actor getActor() {
        return this.actorDAO.findByEmail(getEmail()).orElse(null);
    }

    private String extractEmail() {
        final Map<String, Object> claims = lookupClaims();
        return Arrays.stream(EMAIL_KEYS)
                .filter(k -> claims.containsKey(k))
                .map(claims::get).map(Object::toString)
                .findFirst().orElse(null);
    }

    private Map<String, Object> lookupClaims() {
        OAuth2AccessToken accessToken = lookupAccessToken();
        return accessToken.getAdditionalInformation();
    }

    private OAuth2AccessToken lookupAccessToken() {
        final OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        final OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
        return tokenStore.readAccessToken(details.getTokenValue());
    }

    private boolean isAuthenticated() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication != null && authentication.isAuthenticated();
    }
}
