package fr.hohou.cashier.actor.service;

import fr.hohou.cashier.actor.domain.Actor;

public interface ActorService {

    Actor getCurrentActorDetails();

}
