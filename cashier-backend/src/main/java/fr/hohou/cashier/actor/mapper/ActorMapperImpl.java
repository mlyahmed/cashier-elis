package fr.hohou.cashier.actor.mapper;

import fr.hohou.cashier.actor.domain.Actor;
import fr.hohou.cashier.actor.domain.ActorBean;
import org.springframework.stereotype.Component;

@Component
public class ActorMapperImpl implements ActorMapper {

    @Override
    public ActorBean toBean(Actor actor) {
        if (actor == null) return null;
        return ActorBean.newActorBean()
                .setId(actor.getId())
                .setEmail(actor.getEmail());
    }

}
