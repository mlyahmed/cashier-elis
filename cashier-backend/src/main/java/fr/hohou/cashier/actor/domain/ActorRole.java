package fr.hohou.cashier.actor.domain;

public enum ActorRole {
    OPERATOR, ADMIN
}
