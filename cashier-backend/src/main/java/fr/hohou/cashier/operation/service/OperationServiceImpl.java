package fr.hohou.cashier.operation.service;

import fr.hohou.cashier.operation.dao.OperationDAO;
import fr.hohou.cashier.operation.domain.Operation;
import fr.hohou.cashier.operation.domain.OperationStatus;
import fr.hohou.cashier.operation.domain.OperationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OperationServiceImpl implements OperationService {

    private final OperationDAO operationDAO;

    @Autowired
    public OperationServiceImpl(OperationDAO operationDAO) {
        this.operationDAO = operationDAO;
    }


    @Override
    public Operation cashIn(Operation operation) {
        //TODO : Validations come when the test set comes.
        operation.setType(OperationType.CASH_IN);
        operation.setStatus(OperationStatus.ONGOING);
        return this.operationDAO.save(operation);
    }

}
