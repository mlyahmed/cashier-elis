package fr.hohou.cashier.operation.domain;

public enum OperationType {
    CASH_IN, CASH_OUT
}
