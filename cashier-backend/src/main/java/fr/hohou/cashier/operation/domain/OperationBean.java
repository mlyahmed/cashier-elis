package fr.hohou.cashier.operation.domain;

import fr.hohou.cashier.amount.Amount;

public class OperationBean {

    private String id;
    private String accountId;
    private OperationType type;
    private Amount amount;
    private OperationStatus status;


    public static OperationBean newOperationBean() {
        return new OperationBean();
    }

    public OperationBean() {

    }

    public String getId() {
        return id;
    }

    public OperationBean setId(String id) {
        this.id = id;
        return this;
    }

    public String getAccountId() {
        return accountId;
    }

    public OperationBean setAccountId(String accountId) {
        this.accountId = accountId;
        return this;
    }

    public OperationType getType() {
        return type;
    }

    public OperationBean setType(OperationType type) {
        this.type = type;
        return this;
    }

    public Amount getAmount() {
        return amount;
    }

    public OperationBean setAmount(Amount amount) {
        this.amount = amount;
        return this;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public OperationBean setStatus(OperationStatus status) {
        this.status = status;
        return this;
    }
}
