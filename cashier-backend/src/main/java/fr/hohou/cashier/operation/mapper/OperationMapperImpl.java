package fr.hohou.cashier.operation.mapper;

import fr.hohou.cashier.account.dao.AccountDAO;
import fr.hohou.cashier.operation.domain.Operation;
import fr.hohou.cashier.operation.domain.OperationBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OperationMapperImpl implements OperationMapper {

    private final AccountDAO accountDAO;


    @Autowired
    public OperationMapperImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public Operation toEntity(OperationBean operation) {
        if (operation == null) return null;

        return Operation.newOperation()
                .setId(operation.getId())
                .setAccount(this.accountDAO.findById(operation.getAccountId()).orElse(null))
                .setAmount(operation.getAmount())
                .setStatus(operation.getStatus())
                .setType(operation.getType());

    }

    @Override
    public OperationBean toBean(Operation operation) {
        if (operation == null) return null;

        return OperationBean.newOperationBean()
                .setId(operation.getId())
                .setAccountId(operation.getAccount().getId()) //FIXME : What if the account is null ? Please fail it by a test first.
                .setAmount(operation.getAmount())
                .setStatus(operation.getStatus())
                .setType(operation.getType());
    }

}
