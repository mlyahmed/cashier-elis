package fr.hohou.cashier.operation.api;

import fr.hohou.cashier.operation.domain.OperationBean;
import fr.hohou.cashier.operation.mapper.OperationMapper;
import fr.hohou.cashier.operation.service.OperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/operation")
public class OperationController {

    private final OperationMapper operationMapper;
    private final OperationService operationService;

    @Autowired
    public OperationController(OperationMapper operationMapper, OperationService operationService) {
        this.operationMapper = operationMapper;
        this.operationService = operationService;
    }

    @PostMapping(path = "/v1/cash-in", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasPermission(#operation, 'CREATE')")
    public @ResponseBody
    OperationBean createOperation(@RequestBody OperationBean operation) {
        return operationMapper.toBean(operationService.cashIn(operationMapper.toEntity(operation)));
    }

}
