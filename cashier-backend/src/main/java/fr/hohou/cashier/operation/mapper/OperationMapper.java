package fr.hohou.cashier.operation.mapper;

import fr.hohou.cashier.operation.domain.Operation;
import fr.hohou.cashier.operation.domain.OperationBean;

public interface OperationMapper {

    Operation toEntity(OperationBean operation);

    OperationBean toBean(Operation operation);

}
