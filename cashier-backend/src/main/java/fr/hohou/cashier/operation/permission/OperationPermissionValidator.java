package fr.hohou.cashier.operation.permission;

import fr.hohou.cashier.account.domain.Account;
import fr.hohou.cashier.account.dao.AccountDAO;
import fr.hohou.cashier.operation.domain.OperationBean;
import fr.hohou.cashier.actor.service.CurrentActor;
import fr.hohou.cashier.security.MethodPermission;
import fr.hohou.cashier.security.ResourcePermissionValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OperationPermissionValidator implements ResourcePermissionValidator<OperationBean> {

    @Autowired
    private CurrentActor currentActor;

    @Autowired
    private AccountDAO accountDAO;

    @Override
    public boolean hasPermission(Authentication authentication, OperationBean operation, MethodPermission permission) {
        if (operation == null) return true;
        if (permission == MethodPermission.CREATE && operation.getAccountId() == null) return true;

        final Optional<Account> account = accountDAO.findById(operation.getAccountId());
        if (account.isEmpty()) return true;

        return account.get().getOwner().getEmail().equals(currentActor.getEmail());
    }

    @Override
    public Class<OperationBean> support() {
        return OperationBean.class;
    }
}
