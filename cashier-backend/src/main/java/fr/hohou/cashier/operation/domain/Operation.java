package fr.hohou.cashier.operation.domain;

import fr.hohou.cashier.CashierEntity;
import fr.hohou.cashier.account.domain.Account;
import fr.hohou.cashier.amount.Amount;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FINANCIAL_MOVEMENT")
public class Operation implements CashierEntity {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "ID", nullable = false, unique = true)
    private String id;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;

    @Column(name = "TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    private OperationType type;

    @Column(name = "AMOUNT", nullable = false)
    @Type(type = AMOUNT_TYPE)
    private Amount amount;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(EnumType.STRING)
    private OperationStatus status;


    public static Operation newOperation() {
        return new Operation();
    }


    public String getId() {
        return id;
    }

    public Operation setId(String id) {
        this.id = id;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public Operation setAccount(Account account) {
        this.account = account;
        return this;
    }

    public OperationType getType() {
        return type;
    }

    public Operation setType(OperationType type) {
        this.type = type;
        return this;
    }

    public Amount getAmount() {
        return amount;
    }

    public Operation setAmount(Amount amount) {
        this.amount = amount;
        return this;
    }

    public OperationStatus getStatus() {
        return status;
    }

    public Operation setStatus(OperationStatus status) {
        this.status = status;
        return this;
    }
}
