package fr.hohou.cashier.operation.domain;

public enum OperationStatus {
    ONGOING, COMMITTED, CANCELED
}
