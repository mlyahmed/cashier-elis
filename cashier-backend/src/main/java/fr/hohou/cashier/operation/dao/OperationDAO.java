package fr.hohou.cashier.operation.dao;

import fr.hohou.cashier.operation.domain.Operation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OperationDAO extends JpaRepository<Operation, String> {
}
