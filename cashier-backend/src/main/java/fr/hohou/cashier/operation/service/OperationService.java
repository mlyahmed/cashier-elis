package fr.hohou.cashier.operation.service;

import fr.hohou.cashier.operation.domain.Operation;

public interface OperationService {

    Operation cashIn(Operation operation);

}
