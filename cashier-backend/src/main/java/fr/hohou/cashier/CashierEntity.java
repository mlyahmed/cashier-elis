package fr.hohou.cashier;

import fr.hohou.cashier.amount.AmountJpaType;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@TypeDefs(value = {
        @TypeDef(name = CashierEntity.AMOUNT_TYPE, typeClass = AmountJpaType.class, defaultForType = AmountJpaType.class)
})
@MappedSuperclass
public interface CashierEntity extends Serializable {
    String AMOUNT_TYPE = "Amount";
}
