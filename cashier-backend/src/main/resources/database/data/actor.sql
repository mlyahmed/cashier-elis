--liquibase formatted sql

--changeset ahmed.elidrissi:100 runOnChange:true
DELETE FROM ACTOR WHERE ID='3b311716-22a5-4c92-8242-23dca2d5f8fb';
INSERT INTO ACTOR (ID, EMAIL) VALUES ('3b311716-22a5-4c92-8242-23dca2d5f8fb', 'ahmed.elidrissi.attach@gmail.com');

--changeset ahmed.elidrissi:101 runOnChange:true
DELETE FROM ACTOR_ROLE WHERE ACTOR_ID='3b311716-22a5-4c92-8242-23dca2d5f8fb';
INSERT INTO ACTOR_ROLE (ACTOR_ID, ROLE_CODE) VALUES ('3b311716-22a5-4c92-8242-23dca2d5f8fb', 'ADMIN');
INSERT INTO ACTOR_ROLE (ACTOR_ID, ROLE_CODE) VALUES ('3b311716-22a5-4c92-8242-23dca2d5f8fb', 'OPERATOR');