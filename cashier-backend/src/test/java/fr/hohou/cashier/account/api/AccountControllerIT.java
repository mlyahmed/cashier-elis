package fr.hohou.cashier.account.api;

import fr.hohou.cashier.AbstractIntegrationTest;
import fr.hohou.cashier.account.dao.AccountDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AccountControllerIT extends AbstractIntegrationTest {

    @Autowired
    private AccountDAO accountDAO;

    @Test
    void when_get_an_actor_account_then_return_it() throws Exception {
        when_get_an_actor_account(ROOT_ACTOR_ID);
        then_it_is_returned(ROOT_ACTOR_ID);
    }

    private void when_get_an_actor_account(String actorId) throws Exception {
        this.result = this.mockMvc.perform(
                get("/api/account/v1/by/actorId/{actorId}", actorId)
                        .header("Authorization", "Bearer " + tokenHelper.obtainAccessToken(ROOT_ACTOR_EMAIL))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
        );
    }

    private void then_it_is_returned(String actorId) throws Exception {
        this.result.andExpect(status().is(HttpStatus.OK.value())).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        this.result.andExpect(jsonPath("$.id", equalTo(accountDAO.findByOwnerId(actorId).get().getId())));
        this.result.andExpect(jsonPath("$.currentBalance.value", notNullValue()));
        this.result.andExpect(jsonPath("$.comingBalance.value", notNullValue()));
    }


}
