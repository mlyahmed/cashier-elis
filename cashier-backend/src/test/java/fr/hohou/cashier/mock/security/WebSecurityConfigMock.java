package fr.hohou.cashier.mock.security;

import fr.hohou.cashier.actor.domain.Actor;
import fr.hohou.cashier.actor.dao.ActorDAO;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@EnableWebSecurity
@Configuration
public class WebSecurityConfigMock extends WebSecurityConfigurerAdapter {


    public static final String PASSWORD_FOR_TEST = "dummyPassword";

    private static final int ENCODING_STRENGTH = 4;

    @Autowired
    private ActorDAO actorDAO;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(ENCODING_STRENGTH);
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(actorDAO.findAll().stream().map(this::toUser).toArray(UserDetails[]::new));
    }

    @NotNull
    private UserDetails toUser(Actor a) {
        return User.builder()
                .passwordEncoder(passwordEncoder()::encode)
                .username(a.getEmail())
                .password(PASSWORD_FOR_TEST)
                .roles(a.getRoles().stream().map(Enum::name).toArray(String[]::new))
                .build();
    }

    @Bean
    public DaoAuthenticationProvider daoAuthProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService());
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }



    @Autowired
    public void configAuthenticationProvider(final AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(daoAuthProvider());
    }



    @Bean("authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
