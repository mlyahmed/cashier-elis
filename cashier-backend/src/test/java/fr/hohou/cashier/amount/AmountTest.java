package fr.hohou.cashier.amount;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Testing The Amount Entity")
class AmountTest {

    private static Stream<String> badValueExamples() {
        return Stream.of(
                "12,23",
                "asd",
                "123 243",
                "652-872"
        );
    }

    private static Stream<Arguments> stringToStringExamples() {
        return Stream.of(
                Arguments.of("12.49", "12.49"),
                Arguments.of("10", "10.00"),
                Arguments.of("5.92", "5.92"),
                Arguments.of("0.85", "0.85")
        );
    }

    private static Stream<Arguments> stringToDoubleExamples() {
        return Stream.of(
                //CHECKSTYLE:OFF
                Arguments.of("12.49", 12.49),
                Arguments.of("10", 10.00),
                Arguments.of("5.92", 5.92),
                Arguments.of("0.85", 0.85)
                //CHECKSTYLE:ON
        );
    }

    private static Stream<String> emptyValues() {
        return Stream.of(null, "");
    }

    private static Stream<Arguments> sumExamples() {
        return Stream.of(
                Arguments.of("1", "2", "3"),
                Arguments.of("1.23", "200.52", "201.75"),
                Arguments.of("23.98", "09.76", "33.74"),
                Arguments.of("23.98", "-09.76", "14.22")
        );
    }

    private static Stream<Arguments> multiplyByIntExamples() {
        return Stream.of(
                //CHECKSTYLE:OFF
                Arguments.of("1", 2, "2"),
                Arguments.of("1.23", 200, "246.00"),
                Arguments.of("23.98", 76, "1822.48"),
                Arguments.of("36.98", -6, "-221.88")
                //CHECKSTYLE:ON
        );
    }

    @ParameterizedTest
    @MethodSource("badValueExamples")
    void when_create_and_value_is_not_valid_then_error(String value) {
        final BadAmountValueException exc = assertThrows(BadAmountValueException.class, () -> new Amount(value));
        assertThat(exc.getMessage(), equalTo(String.format("The Value <%s> is not valid", value)));
    }

    @ParameterizedTest
    @MethodSource("stringToStringExamples")
    void when_set_value_then_get_it_is_string(String value, String result) {
        Amount amount = new Amount(value);
        assertThat(amount.asString(), equalTo(result));
    }

    @ParameterizedTest
    @MethodSource("stringToDoubleExamples")
    void when_set_value_then_get_it_is_double(String value, double result) {
        Amount amount = new Amount(value);
        assertThat(amount.asDouble(), equalTo(result));
    }

    @ParameterizedTest
    @MethodSource("emptyValues")
    void when_value_is_empty_then_it_s_value_is_zero(String empty) {
        Amount amount = new Amount(empty);
        assertThat(amount.asDouble(), equalTo(0.00));
    }

    @ParameterizedTest
    @MethodSource("sumExamples")
    void when_add_two_values_then_return_the_right_result(String f, String s, String r) {
        Amount first = new Amount(f);
        Amount second = new Amount(s);
        Amount result = first.plus(second);
        assertThat(result, equalTo(new Amount(r)));
        assertThat(second, equalTo(new Amount(s)));
        assertThat(first, equalTo(new Amount(f)));
    }

    @ParameterizedTest
    @MethodSource("multiplyByIntExamples")
    void when_multiply_by_an_integer_then_return_the_right_result(String f, int s, String r) {
        Amount first = new Amount(f);
        Amount result = first.multiply(s);
        assertThat(result, equalTo(new Amount(r)));
    }

    @ParameterizedTest
    @MethodSource("multiplyByIntExamples")
    void when_multiply_by_a_double_then_return_the_right_result(String f, double s, String r) {
        Amount first = new Amount(f);
        Amount result = first.multiply(s);
        assertThat(result, equalTo(new Amount(r)));
    }

}
