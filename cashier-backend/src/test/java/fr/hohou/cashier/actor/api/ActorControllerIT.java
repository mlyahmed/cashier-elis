package fr.hohou.cashier.actor.api;

import fr.hohou.cashier.AbstractIntegrationTest;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ActorControllerIT  extends AbstractIntegrationTest {

    @Test
    void when_get_current_actor_details_then_return_them() throws Exception {
        when_get_current_actor_details(ROOT_ACTOR_EMAIL);
        then_the_details_are_returned(ROOT_ACTOR_ID, ROOT_ACTOR_EMAIL);
    }

    private void then_the_details_are_returned(String actorId, String actorEmail) throws Exception {
        this.result.andExpect(status().is(HttpStatus.OK.value())).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        this.result.andExpect(jsonPath("$.id", equalTo(actorId)));
        this.result.andExpect(jsonPath("$.email", equalTo(actorEmail)));
    }

    private void when_get_current_actor_details(String actorEmail) throws Exception {
        this.result = this.mockMvc.perform(
                get("/api/actor/v1/current")
                        .header("Authorization", "Bearer " + tokenHelper.obtainAccessToken(actorEmail))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
        );
    }

}
