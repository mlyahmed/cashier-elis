package fr.hohou.cashier;

import fr.hohou.cashier.mock.security.WebSecurityConfigMock;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.UnsupportedEncodingException;

import static fr.hohou.cashier.mock.security.AuthorizationServerConfigMock.CLIENT_ID;
import static fr.hohou.cashier.mock.security.AuthorizationServerConfigMock.CLIENT_SECRET;
import static org.apache.commons.codec.binary.Base64.encodeBase64;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Component
public class TokenHelperForTest {

    @Autowired
    protected MockMvc mockMvc;

    public String obtainAccessToken(String username) {
        try {

            ResultActions result = mockMvc.perform(post("/oauth/token")
                    .params(buildParams(username))
                    .header("Authorization", "Basic " + encodeClientCredentials())
                    .accept("application/json;charset=UTF-8")).andExpect(status().isOk());
            return extractAccessToken(result);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @NotNull
    private MultiValueMap<String, String> buildParams(String username) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("username", username);
        params.add("password", WebSecurityConfigMock.PASSWORD_FOR_TEST);
        params.add("grant_type", "password");
        params.add("scope", "openid profile email");
        return params;
    }

    @NotNull
    private String encodeClientCredentials() {
        return new String(encodeBase64(String.format("%s:%s", CLIENT_ID, CLIENT_SECRET).getBytes()));
    }

    private String extractAccessToken(ResultActions result) throws UnsupportedEncodingException {
        final String resultString = result.andReturn().getResponse().getContentAsString();
        return new JacksonJsonParser().parseMap(resultString).get("access_token").toString();
    }
}
