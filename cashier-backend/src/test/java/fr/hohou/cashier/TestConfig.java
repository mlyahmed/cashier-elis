package fr.hohou.cashier;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.testcontainers.containers.MariaDBContainer;

import javax.validation.constraints.NotNull;

@Configuration
@Profile("test")
public class TestConfig {

    private static final MariaDBContainer DB_CONTAINER = new MariaDBContainer("mariadb:10.3.6");

    static {
        DB_CONTAINER.start();
    }

    @Bean(destroyMethod = "stop")
    public MariaDBContainer databaseContainer() {
        return DB_CONTAINER;
    }


    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(ConfigurableApplicationContext appConf) {
            TestPropertyValues.of(
                    dbDriver(),
                    dbUrl(),
                    dbUsername(),
                    dbPassword(),
                    security_oauth2_resource_id()
            ).applyTo(appConf.getEnvironment());
        }

        @NotNull
        private String dbPassword() {
            return "spring.datasource.password=" + DB_CONTAINER.getPassword();
        }

        @NotNull
        private String dbUsername() {
            return "spring.datasource.username=" + DB_CONTAINER.getUsername();
        }

        @NotNull
        private String dbUrl() {
            return "spring.datasource.url=" + DB_CONTAINER.getJdbcUrl();
        }

        private String dbDriver() {
            return "spring.datasource.driver-class-name=" + DB_CONTAINER.getDriverClassName();
        }


        private String security_oauth2_resource_id() {
            return "security.oauth2.resource.id=resourceId";
        }

    }

}
