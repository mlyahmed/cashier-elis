package fr.hohou.cashier;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Transactional
@ContextConfiguration(initializers = { TestConfig.Initializer.class })
@Import(TestConfig.class)
public class AbstractIntegrationTest {

    public static final String ROOT_ACTOR_EMAIL = "ahmed.elidrissi.attach@gmail.com";
    public static final String ROOT_ACCOUNT_ID = "9e86a53c-8b6e-4eea-83b2-f469fcce23e7";
    public static final String ROOT_ACTOR_ID = "3b311716-22a5-4c92-8242-23dca2d5f8fb";
    @Autowired
    protected ObjectMapper mapper;

    @Autowired
    protected MockMvc mockMvc;

    protected ResultActions result;

    @Autowired
    protected TokenHelperForTest tokenHelper;

}
