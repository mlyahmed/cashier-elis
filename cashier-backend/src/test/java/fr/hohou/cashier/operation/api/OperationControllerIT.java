package fr.hohou.cashier.operation.api;

import fr.hohou.cashier.AbstractIntegrationTest;
import fr.hohou.cashier.operation.domain.Operation;
import fr.hohou.cashier.operation.domain.OperationBean;
import fr.hohou.cashier.operation.domain.OperationStatus;
import fr.hohou.cashier.operation.domain.OperationType;
import fr.hohou.cashier.operation.dao.OperationDAO;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class OperationControllerIT extends AbstractIntegrationTest {

    @Autowired
    private OperationDAO operationDAO;

    @ParameterizedTest
    @ValueSource(strings = {"22.33", "33.98", "999.98"})
    void when_cash_in_then_it_is_committed(String amount) throws Exception {
        when_cash_in(ROOT_ACCOUNT_ID, amount);
        then_the_cash_in_is_committed(ROOT_ACCOUNT_ID, amount);
    }

    @ParameterizedTest
    @ValueSource(strings = {"09.34", "176.76", "1999.01"})
    void when_cash_in_then_it_is_saved(String amount) throws Exception {
        when_cash_in(ROOT_ACCOUNT_ID, amount);
        then_the_cash_in_is_saved();
    }


    private void when_cash_in(String accountId, String amount) throws Exception {
        final Map<String, Object> query = new HashMap<>();
        query.put("accountId", accountId);
        query.put("amount", Map.of("value", amount));

        this.result = this.mockMvc.perform(
                post("/api/operation/v1/cash-in")
                        .header("Authorization", "Bearer " + tokenHelper.obtainAccessToken(ROOT_ACTOR_EMAIL))
                        .content(mapper.writeValueAsString(query))
                        .contentType(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
        );
    }

    private void then_the_cash_in_is_committed(String accountId, String amount) throws Exception {
        this.result.andExpect(status().is(HttpStatus.CREATED.value())).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        this.result.andExpect(jsonPath("$.id", notNullValue()));
        this.result.andExpect(jsonPath("$.accountId", equalTo(accountId)));
        this.result.andExpect(jsonPath("$.type", equalTo(OperationType.CASH_IN.name())));
        this.result.andExpect(jsonPath("$.amount.value", equalTo(amount)));
        this.result.andExpect(jsonPath("$.status", equalTo(OperationStatus.ONGOING.name())));
    }

    private void then_the_cash_in_is_saved() throws Exception {
        final OperationBean bean = this.mapper.readValue(this.result.andReturn().getResponse().getContentAsString(), OperationBean.class);
        final Optional<Operation> operation = operationDAO.findById(bean.getId());
        assertThat(operation.isPresent(), equalTo(true));
        assertThat(operation.get().getAccount().getId(), equalTo(bean.getAccountId()));
        assertThat(operation.get().getAmount(), equalTo(bean.getAmount()));
        assertThat(operation.get().getStatus(), equalTo(bean.getStatus()));
        assertThat(operation.get().getType(), equalTo(bean.getType()));
    }


}
